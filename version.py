import requests
import sys
from datetime import datetime

response = requests.get("https://pypi.org/pypi/youtube_dl/json")
if not response.ok:
    sys.exit("Fail response")

latest_version = None
latest_upload_time = None
date_format = "%Y-%m-%dT%H:%M:%S"

releases = response.json()["releases"]
for version in releases:
    release = releases[version]
    if len(release) <= 0:
        continue
    upload_time_str = release[0]["upload_time"]
    upload_time = datetime.strptime(upload_time_str, date_format)
    if latest_version is None:
        latest_version = version
        latest_upload_time = upload_time
        continue
    if upload_time > latest_upload_time:
        latest_version = version
        latest_upload_time = upload_time

if latest_version is None:
    sys.exit("Cannot find the latest version")

print(f"The latest version is {latest_version}")

with open("version.txt", "w", encoding="utf-8") as file:
    file.write(latest_version)
